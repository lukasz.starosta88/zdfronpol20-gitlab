// Import funkcji związanych z obsługą Firebase'a
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";

import {
    getAuth,
    signInWithEmailAndPassword,
    createUserWithEmailAndPassword,
    signOut,
    onAuthStateChanged,
    AuthErrorCodes
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-auth.js";

// Przygtowanie konfiguracji projektu Firebase'a
const firebaseConfig = {
    apiKey: "AIzaSyAYZwa5wedKN6oWfJXrvrMZDkiGeeuXzTQ",
    authDomain: "zdfronpol20-arek.firebaseapp.com",
    projectId: "zdfronpol20-arek",
    storageBucket: "zdfronpol20-arek.appspot.com",
    messagingSenderId: "159590018331",
    appId: "1:159590018331:web:e450aa4c0f224547c977be"
};

// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
// - uruchmienie modułu uwierzytelniania
const auth = getAuth(app);

// Definiowanie elementów UI
const emailForm = document.querySelector("#emailForm");
const passwordForm = document.querySelector("#passwordForm");

const signUpBtn = document.querySelector("#signUpBtn");
const signInBtn = document.querySelector("#signInBtn");
const signOutBtn = document.querySelector("#signOutBtn");

const errorLabel = document.querySelector("#errorLabel");

const viewForNotLoggedUser = document.querySelector("#viewForNotLoggedUser");
const viewForLoggedUser = document.querySelector("#viewForLoggedUser");

// OPERACJA 1: Logowanie użytkownika
const signInUser = async () => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;

    try {
        const user = await signInWithEmailAndPassword(auth, valueEmail, valuePassword);
        console.log('User logged in.');
    } catch {
        // Ewentualnie, gdyby skorzystać z catch(exception) to możemy wyjąć
        // później kod błędu (poprzez exception.code)
        // console.log(exception.code);
        errorLabel.innerHTML = "Podano nieprawidłowy email lub hasło";
    }
}

// Dodanie eventu do przycisku SignIn (wywołanie funkcji logoującej użytkownika)
signInBtn.addEventListener("click", signInUser);

// OPERACJA 2: Rejestracja użytkownika
const signUpUser = async () => {
    const valueEmail = emailForm.value;
    const valuePassword = passwordForm.value;

    try {
        const user = await createUserWithEmailAndPassword(auth, valueEmail, valuePassword);
    } catch (error) {
        if (error.code === "auth/email-already-in-use") {
            console.log("wybierz inny email, ten jest zajęty..")
        } else {
            console.log("Rejestracja nie powiodła się...");
        }
    }
    
    // ⚠️ WAŻNE ⚠️
    // wskaźnik -> doc(database, "uzytkownicy", user.user.uid)
    // Dodanie danych do bazy danych (np. Firestore)
};

// Dodanie eventu do przycisku SignUp (wywołanie funkcji rejrestrującej użytkownika)
signUpBtn.addEventListener("click", signUpUser);

// OPERACJA 3: Wylogowanie użytkownika
const signOutUser = async () => {
    await signOut(auth);
    console.log("User logged out.");
};

// Dodanie eventu to przycisku SignOut (wywołanie funkcji wylogowującej)
signOutBtn.addEventListener("click", signOutUser);

// OPERACJA 4: Nasłuchiwanie na zmianę statusu sesji użytkownika
const authUserObserver = () => {
    onAuthStateChanged(auth, user => {
        if (user) {
            // Ktoś jest zalogowany..
            viewForLoggedUser.style.display = 'block';
            viewForNotLoggedUser.style.display = 'none';
        } else {
            // Ktoś nie jest zalogowany..
            viewForLoggedUser.style.display = 'none';
            viewForNotLoggedUser.style.display = 'block';
        }
    });
}
authUserObserver();