// // Import funkcji związanych z obsługą Firebase'a
// import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";
// import {
//   getFirestore,
//   doc,
//   getDoc,
//   setDoc,
//   addDoc,
//   collection,
//   query,
//   getDocs,
// } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-firestore.js";

// // Przygtowanie konfiguracji projektu Firebase'a
// const firebaseConfig = {
//   apiKey: "AIzaSyAYZwa5wedKN6oWfJXrvrMZDkiGeeuXzTQ",
//   authDomain: "zdfronpol20-arek.firebaseapp.com",
//   projectId: "zdfronpol20-arek",
//   storageBucket: "zdfronpol20-arek.appspot.com",
//   messagingSenderId: "159590018331",
//   appId: "1:159590018331:web:e450aa4c0f224547c977be",
// };

// // Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// // - uruchomienie aplikacji
// const app = initializeApp(firebaseConfig);
// // - uruchomienie modułu Firestore (baza danych)
// const database = getFirestore(app);

// CRUD -> Create, Read, Update, Delete

// READ: pobieranie danych z bazy danych

// OPERACJA 1: Pobieranie konkretnego dokumentu (w naszym przypadku produktu)
// 1.1: Stworzenie wskaźnika, o jaki element nam chodzi
// const docRef = doc(database, "produkty", "mysz C7");
// const docSnap = await getDoc(docRef);
// console.log(docSnap.data()); -> Pokazuje wszystkie pola z produktu
// console.log(docSnap.data().cena); -> Pokazuje tylko cenę produktu

// // 1.2: Sprawdzenie, czy dokument istnieje?
// //      Dobry zwyczaj, gdy wyciągamy jeden konkretny dokument.
// if (docSnap.exists()) {
//   console.log(docSnap.data());
// } else {
//   console.log("Dokument nie istnieje!");
// }

// // CREATE: tworzenie i zapis danych do bazy danych
// // OPERACJA 2: Dodanie pojedynczego dokumentu do bazy danych
// // 2.1: Utworzenie ręczne wskaźnika (ścieżki), gdzie chcemy zapisać dokument
// // Dodaje (jeśli nie istnieje) lub nadpisuje (gdy wpis już istniał)
// const setDocRef = doc(database, "produkty", "monitor e2");
// const monitorProperties = {
//   nazwa: "Monitor przenośny e2",
//   cena: 500,
//   czy_dostepny: false,
// };
// await setDoc(setDocRef, monitorProperties);

// // 2.2: Utworzenie automatyczne wskaźnika (ścieżki), gdzie chcemy zapisać dokument
// // Dodaje za każdym razem nowy wpis
// const productsCollectionRef = collection(database, "produkty");
// const webcamProperties = {
//   nazwa: "Webcam v2",
//   cena: 200,
//   czy_dostepny: true,
// };
// const createdProduct = await addDoc(productsCollectionRef, webcamProperties);
// const createdProductId = createdProduct.id;

// const createdProductRef = doc(database, "produkty", createdProductId);
// const createdProductSnap = await getDoc(createdProductRef);
// console.log(createdProductSnap.data());

// // READ ALL: pobieranie wszystkich elementów z kolekcji
// // OPERACJA 3: odczytanie wszystkich elementów z kolekcji

// // 3.1. Wskazanie kolekcji, z której chcemy pobrać dane
// const queryAllProducts = query(collection(database, "produkty"));
// // const queryAllProducts = query(productsCollectionRef); -> to jest to samo, co wyżej
// const allProductsSnap = await getDocs(queryAllProducts);

// allProductsSnap.forEach((singleProduct) => {
//   console.log(singleProduct.data().nazwa);
// });

import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";
import {
  getFirestore,
  doc,
  getDoc,
  setDoc,
  addDoc,
  collection,
  query,
  getDocs,
  where,
  orderBy,
  limit,
  deleteDoc,
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-firestore.js";

const firebaseConfig = {
  apiKey: "AIzaSyDOQAsy4hmFfRdU0PVLpr9gZNPFYVFROsc",
  authDomain: "zdfronpol20-zwierzeta.firebaseapp.com",
  projectId: "zdfronpol20-zwierzeta",
  storageBucket: "zdfronpol20-zwierzeta.appspot.com",
  messagingSenderId: "1005707998234",
  appId: "1:1005707998234:web:2e53a10c8d0069e0e7cda3",
};

const app = initializeApp(firebaseConfig);

const database = getFirestore(app);

// 7. Dopisz funkcjonalność, która wyświetli w konsoli informacje na temat zwierzęcia o
// ID=1.

const docRef = doc(database, "Zwierzeta", "1");
const docSnap = await getDoc(docRef);

console.log(docSnap.data());

// 8. Dopisz funkcjonalność, która wyświetli w konsoli nazwy wszystkich dostępnych
// zwierząt.

// const docRef = doc(database, "Zwierzeta");
// const docSnap = await getDoc(docRef);

// console.log(docSnap.data());

const queryAllPets = query(collection(database, "Zwierzeta"));
const allProductsSnap = await getDocs(queryAllPets);
allProductsSnap.forEach((element) => {
  console.log(element.data());
});

// 9. Dopisz funkcjonalność, która doda nowe zwierzę do bazy danych (możesz przyjąć
//     stałe ID). Potwierdzniem poprawnego działania tego punktu będzie pobranie
//     danych z bazy dla tego zwierzęcia oraz wyświetlenie tych danych w konsoli w
//     przeglądarce

// const setDocRef = doc(database, "Zwierzeta", "3");
// const newPet = {
//   nazwa: "pies Azor",
//   cena: 300,
//   wymagane_pozwolenie: false,
// };
// await setDoc(setDocRef, newPet);

// // CREATE: tworzenie i zapis danych do bazy danych
// // OPERACJA 2: Dodanie pojedynczego dokumentu do bazy danych
// // 2.1: Utworzenie ręczne wskaźnika (ścieżki), gdzie chcemy zapisać dokument
// // Dodaje (jeśli nie istnieje) lub nadpisuje (gdy wpis już istniał)
// const setDocRef = doc(database, "produkty", "monitor e2");
// const monitorProperties = {
//   nazwa: "Monitor przenośny e2",
//   cena: 500,
//   czy_dostepny: false,
// };
// await setDoc(setDocRef, monitorProperties);

// OPERACJA 1: Pobieranie konkretnego dokumentu (w naszym przypadku produktu)
// 1.1: Stworzenie wskaźnika, o jaki element nam chodzi
// const docRef = doc(database, "produkty", "mysz C7");
// const docSnap = await getDoc(docRef);
// console.log(docSnap.data()); -> Pokazuje wszystkie pola z produktu
// console.log(docSnap.data().cena); -> Pokazuje tylko cenę produktu

// Zadanie dalsze - to jest z bloku teoretycznego

// 9. Dopisz funkcjonalność, która wyświetli w konsoli nazwy wszystkich zwierząt,
// których adopcja wymaga pozwolenia (wymagane_pozwolenie=true).
const queryAllAvailablePets = query(
  collection(database, "Zwierzeta"),
  where("wymagane_pozwolenie", "==", true)
);
const allAvailablePets = await getDocs(queryAllAvailablePets);
allAvailablePets.forEach((Zwierze) => {
  console.log(Zwierze.data());
});

// 10. Dopisz funkcjonalność, która wyświetli w konsoli nazwy oraz ceny wszystkich
// dostępnych zwierząt w kolejności od najdroższych do najtańszych.

const queryAllPetsSortedByPriceAscending = query(
  collection(database, "Zwierzeta"),
  orderBy("cena"),
  limit(3)
);

const allPetsSortedByPriceAsceding = await getDocs(
  queryAllPetsSortedByPriceAscending
);
allPetsSortedByPriceAsceding.forEach((ZwierzetaAscending) => {
  console.log(
    `${ZwierzetaAscending.data().nazwa}: ${ZwierzetaAscending.data().cena} PLN`
  );
});

// 11. Dopisz funkcjonalność, która doda nowe zwierzę do bazy danych (możesz przyjąć
//   stałe ID). Potwierdzniem poprawnego działania tego punktu będzie pobranie
//   danych z bazy dla tego zwierzęcia oraz wyświetlenie tych danych w konsoli w
//   przeglądarce.

const setDocRef1 = doc(database, "Zwierzeta", "4");
const newPet1 = {
  nazwa: "chomik Tadek",
  cena: 50,
  wymagane_pozwolenie: false,
};
await setDoc(setDocRef1, newPet1);
console.log(newPet1);

// 12. Dopisz funkcjonalność, która zmodyfikuje cenę zwierzęcia dodanego w
// poprzednim punkcie. Ze względu na promocję cenę należy obniżyć o 30%.
// Potwierdzniem poprawnego działania tego punktu będzie pobranie danych z
// bazy dla tego zwierzęcia oraz wyświetlenie tych danych w konsoli w
// przeglądarce.

const petToUpdateNewPrice = doc(database, "Zwierzeta", "chomik Tadek");
const petToUpdateDiscount = {
  cena: 35,
};
console.log(petToUpdateDiscount);

// 13. Dopisz funkcjonalność, która usunie zwierzę dodane w punkcie 11.
// Potwierdzniem poprawnego działania tego punktu będzie pobranie listy
// wszystkich zwierząt z danych, wyświetlenie ich nazw oraz sprawdzenie, że
// nazwa usuniętego zwierzęcia już się nie pojawia.
const petToBeDeleted = doc(database, "Zwierzeta", "4");
await deleteDoc(petToBeDeleted);

const queryAllPets2 = query(collection(database, "Zwierzeta"));
const allProductsSnap2 = await getDocs(queryAllPets2);
allProductsSnap2.forEach((element) => {
  console.log(element.data());
});

// 14. * Dopisz funkcjonalność, która doda UI wyświetlający nazwy oraz ceny
// wszystkich zwierząt.
// a. Forma dowolna, może to być lista, może to być tabelka, może być
// cokolwiek innego - decyzja należy do osoby programującej
// funkcjonalność.

// To co poniżej nie wyszło
// const queryAllAvailablePetsHTML = query(collection(database, "Zwierzeta"));
// const allAvailablePetsHTML = await getDocs(queryAllAvailablePetsHTML);

// let availablePetsListHTML = "<ul>";
// allAvailablePetsHTML.forEach((Zwierze) => {
//   availablePetsListHTML += `<li>${Zwierze.data().nazwa}: ${
//     Zwierze.data().cena
//   } PLN</li>`;
// });
// availablePetsListHTML += "</ul>";

// const availablePetsDiv = document.querySelector("#availablePets");

// console.log(availablePetsListHTML);

const generateAllAnimalsView = async () => {
  // Zadanie 14
  // 14.1: Pobranie wszystkich zwierząt z bazy danych
  const queryAllAnimals = query(collection(database, "Zwierzeta"));
  const allAnimals = await getDocs(queryAllAnimals);

  // 14.2: Wygenerowanie fragmentu HTML
  let allAnimalsView = "<ol>";

  allAnimals.forEach((animal) => {
    allAnimalsView += `<li>
      ${animal.data().nazwa}: ${animal.data().cena} zł
      <button type="button" data-id=${animal.id} class="btnDelete">Usuń</button>
      </li>`;
  });

  allAnimalsView += "</ol>";

  // 14.3: Wrzucenie stworzonego widoku na stronę
  const allAnimalsDiv = document.querySelector("#allAnimals");
  allAnimalsDiv.innerHTML = allAnimalsView;
};

// Zadanie 15
// 15.1: Dodanie przycisku "Usuń" obok ceny ✅

// 15.2: Dodanie do każdego przycisku akcji usuwania danego zwierzęcia

// 15.2.1: Dodanie klasy do przycisków, żeby potem złapać je przez JS ✅

// 15.2.2: Wyłapanie przycisków z HTMLa ✅
const btnsDelete = document.querySelectorAll(".btnDelete");

// 15.2.3: Dodanie funkcji odpowiedzialnej za usuwanie zwierząt z bazy ✅
const deleteAnimalFromDatabse = async (event) => {
  const animalId = event.target.dataset.id;
  const animalRef = doc(database, "Zwierzeta", animalId);
  await deleteDoc(animalRef);

  // 15.3: Po zakończeniu usuwania należy odświeżyć listę widocznych zwierząt
};

// 15.2.4: Dodanie akcji usuwania do przycisków ✅
btnsDelete.forEach((btn) => {
  // TODO: Dodać funkcję usuwającą zwierzęta
  btn.addEventListener("click", deleteAnimalFromDatabse);
});
