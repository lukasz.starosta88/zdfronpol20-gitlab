// // Import funkcji związanych z obsługą Firebase'a
// import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";

// import {
//   getAuth,
//   signInWithEmailAndPassword,
//   createUserWithEmailAndPassword,
//   signOut,
//   GoogleAuthProvider,
//   signInWithPopup,
// } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-auth.js";

// import {
//   getFirestore,
//   doc,
//   getDoc,
//   setDoc,
//   addDoc,
//   collection,
//   query,
//   getDocs,
//   where,
//   orderBy,
//   limit,
//   updateDoc,
//   deleteDoc,
// } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-firestore.js";

// // Przygtowanie konfiguracji projektu Firebase'a
// const firebaseConfig = {
//   apiKey: "AIzaSyDOQAsy4hmFfRdU0PVLpr9gZNPFYVFROsc",
//   authDomain: "zdfronpol20-zwierzeta.firebaseapp.com",
//   projectId: "zdfronpol20-zwierzeta",
//   storageBucket: "zdfronpol20-zwierzeta.appspot.com",
//   messagingSenderId: "1005707998234",
//   appId: "1:1005707998234:web:2e53a10c8d0069e0e7cda3",
// };

// // // Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// // // - uruchomienie aplikacji
// // const app = initializeApp(firebaseConfig);
// // // - uruchmienie modułu uwierzytelniania
// // const auth = getAuth(app);

// // Definiowanie elementów UI
// const emailForm = document.querySelector("#emailForm");
// const passwordForm = document.querySelector("#passwordForm");

// //do nasłuchwiania jakie akcje maja zostac wykonane
// const signUpBtn = document.querySelector("#signUpBtn");
// const signInBtn = document.querySelector("#signInBtn");
// const signOutBtn = document.querySelector("#signOutBtn");
// const signInWithGoogleBtn = document.querySelector("#signInWithGoogleBtn");

// const errorLabel = document.querySelector("#errorLabel");

// const viewForNotLoggedUser = document.querySelector("#viewForNotLoggedUser");
// const viewForLoggedUser = document.querySelector("#viewForLoggedUser");

// // // ----------------------------------------------------
// // // // OPERACJA 1: Logowanie użytkownika
// // // const signInUser = async () => {
// // //   const valueEmail = emailForm.value;
// // //   const valuePassword = passwordForm.value;

// // //   try {
// // //     const user = await signInWithEmailAndPassword(
// // //       auth,
// // //       valueEmail,
// // //       valuePassword
// // //     );
// // //     console.log("User logged in.");
// // //   } catch {
// // //     // Ewentualnie, gdyby skorzystać z catch(exception) to możemy wyjąć
// // //     // później kod błędu (poprzez exception.code)
// // //     // console.log(exception.code);
// // //     errorLabel.innerHTML = "Podano nieprawidłowy email lub hasło";
// // //   }
// // // };

// // // Dodanie eventu do przycisku SignIn (wywołanie funkcji logoującej użytkownika)
// // // signInBtn.addEventListener("click", signInUser);

// // // // OPERACJA 2: Rejestracja użytkownika
// // // const signUpUser = async () => {
// // //   const valueEmail = emailForm.value;
// // //   const valuePassword = passwordForm.value;

// // //   const user = await createUserWithEmailAndPassword(
// // //     auth,
// // //     valueEmail,
// // //     valuePassword
// // //   );

// // //   // :warning: WAŻNE :warning:
// // //   // wskaźnik -> doc(database, "uzytkownicy", user.user.uid)
// // //   // Dodanie danych do bazy danych (np. Firestore)
// // // };

// // // // Dodanie eventu do przycisku SignUp (wywołanie funkcji rejrestrującej użytkownika)
// // // signUpBtn.addEventListener("click", signUpUser);

// // // // OPERACJA 3: Wylogowanie użytkownika
// // // const signOutUser = async () => {
// // //   await signOut(auth);
// // //   console.log("User logged out.");
// // // };

// // // // Dodanie eventu to przycisku SignOut (wywołanie funkcji wylogowującej)
// // // signOutBtn.addEventListener("click", signOutUser);

// // //-----------------------------------
// // //---- ZADANIE SOBOTA 18.02.2023 ----
// // //-----------------------------------

// // // 1. Uruchom moduł Auth w Firestore.
// // // zrobione

// // // 2. Utwórz ręcznie użytkownika w konsoli Firestore.
// // // zrobione

// // // 3. Dopisz funkcjonalność, która pozwoli na uwierzytelnianie użytkownika poprzez
// // // adres mailowy oraz hasło w Auth. Wyświetl dane zalogowanego użytkownika.

// // const app = initializeApp(firebaseConfig);

// // const auth = getAuth(app);

// // // - uruchomienie modułu bez danych
// // const database = getFirestore(app);

// // // 4. Dodaj formularz logowania oraz rejestracji po stronie UI.

// // const signInUser = async () => {
// //   const valueEmail = emailForm.value;
// //   const valuePassword = passwordForm.value;

// //   try {
// //     const user = await signInWithEmailAndPassword(
// //       auth,
// //       valueEmail,
// //       valuePassword
// //     );
// //     console.log("User logged in.");
// //     console.log("abc");
// //   } catch {
// //     errorLabel.innerHTML = "Podano nieprawidłowy email lub hasło";
// //   }
// // };

// // signInBtn.addEventListener("click", signInUser);

// // // a. Formularz powinien zawierać możliwość wpisania maila oraz hasła.
// // // zrobione

// // // 5. Dodaj odpowiednie przyciski logowania, rejestracji oraz wylogowania po stronie UI.
// // // zrobione

// // // 6. Dodaj i przetestuj możliwość logowania użytkownika poprzez UI.
// // // działa

// // // a. Potwierdzniem poprawnego działania tego punktu będzie wyświetlenie w
// // // konsoli danych zalogowanego użytkownika.
// // // chyba zrobione powyżej

// // // 7. Dodaj i przetestuj możliwość rejestracji użytkownika poprzez UI.
// // // a. Potwierdzniem poprawnego działania tego punktu będzie wyświetlenie w
// // // konsoli danych zarejstrowanego użytkownika.

// // const signUpUser = async () => {
// //   const valueEmail = emailForm.value;
// //   const valuePassword = passwordForm.value;

// //   const user = await createUserWithEmailAndPassword(
// //     auth,
// //     valueEmail,
// //     valuePassword
// //   );
// //   console.log("User registred.");
// // };
// // signUpBtn.addEventListener("click", signUpUser);

// // // 8. Dodaj i przetestuj możliwość wylogowania użytkownika poprzez UI.
// // // a. Dodatkowo pokazuj formularz logowania/rejestracji tylko jeśli użytkownik nie
// // // jest zalogowany. Będzie to potwierdzeniem poprawności działania
// // // wylogowywania.

// // const signOutUser = async () => {
// //   await signOut(auth);
// //   console.log("User logged out.");
// // };

// // signOutBtn.addEventListener("click", signOutUser);

// // // 9. Dopisz funkcjonalność, która będzie wyświetlać przycisk Wyloguj tylko jeśli
// // // użytkownik jest w danym momencie zalogowany.
// // const authUserObserver = () => {
// //   onAuthStateChanged(auth, async (user) => {
// //     if (user) {
// //       // Ktoś jest zalogowany..
// //       viewForLoggedUser.style.display = "block";
// //       viewForNotLoggedUser.style.display = "none";

// //       // Zadanie 10
// //       const allAnimalsQuery = query(collection(database, "zwierzeta"));
// //       const allAnimals = await getDocs(allAnimalsQuery);

// //       let allAnimalsView = "<ul>";

// //       allAnimals.forEach((animal) => {
// //         allAnimalsView += `<li>${animal.data().nazwa}</li>`;
// //       });

// //       allAnimalsView += "</ul>";

// //       allAnimalsDiv.innerHTML = allAnimalsView;
// //     } else {
// //       // Ktoś nie jest zalogowany..
// //       viewForLoggedUser.style.display = "none";
// //       viewForNotLoggedUser.style.display = "block";
// //     }
// //   });
// // };
// // authUserObserver();
// // --------------------------------------------------------------------------

// const allAnimalsDiv = document.querySelector("#allAnimals");
// const animalNameForm = document.querySelector("#animalNameForm");
// const createAnimalBtn = document.querySelector("#createAnimalBtn");

// // OPERACJA 1: Logowanie użytkownika
// const signInUser = async () => {
//   const valueEmail = emailForm.value;
//   const valuePassword = passwordForm.value;

//   try {
//     const user = await signInWithEmailAndPassword(
//       auth,
//       valueEmail,
//       valuePassword
//     );

//     // Zadanie 6:
//     console.log(user.user.email);

//     console.log("User logged in.");
//   } catch {
//     // Ewentualnie, gdyby skorzystać z catch(exception) to możemy wyjąć
//     // później kod błędu (poprzez exception.code)
//     // console.log(exception.code);
//     errorLabel.innerHTML = "Podano nieprawidłowy email lub hasło";
//   }
// };

// // Dodanie eventu do przycisku SignIn (wywołanie funkcji logoującej użytkownika)
// signInBtn.addEventListener("click", signInUser);

// // OPERACJA 2: Rejestracja użytkownika
// const signUpUser = async () => {
//   const valueEmail = emailForm.value;
//   const valuePassword = passwordForm.value;

//   try {
//     const user = await createUserWithEmailAndPassword(
//       auth,
//       valueEmail,
//       valuePassword
//     );

//     // Zadanie 7
//     console.log(user);
//   } catch (error) {
//     if (error.code === "auth/email-already-in-use") {
//       console.log("wybierz inny email, ten jest zajęty..");
//     } else {
//       console.log("Rejestracja nie powiodła się...");
//     }
//   }

//   // :warning: WAŻNE :warning:
//   // wskaźnik -> doc(database, "uzytkownicy", user.user.uid)
//   // Dodanie danych do bazy danych (np. Firestore)
// };

// // Dodanie eventu do przycisku SignUp (wywołanie funkcji rejrestrującej użytkownika)
// signUpBtn.addEventListener("click", signUpUser);

// // OPERACJA 3: Wylogowanie użytkownika
// const signOutUser = async () => {
//   await signOut(auth);
//   console.log("User logged out.");
// };

// // Dodanie eventu to przycisku SignOut (wywołanie funkcji wylogowującej)
// signOutBtn.addEventListener("click", signOutUser);

// // Zadanie 10
// const generateAllAnimalsView = async () => {
//   const allAnimalsQuery = query(collection(database, "Zwierzeta"));
//   const allAnimals = await getDocs(allAnimalsQuery);

//   let allAnimalsView = "<ul>";

//   allAnimals.forEach((animal) => {
//     allAnimalsView += `<li>${animal.data().nazwa}</li>`;
//   });

//   allAnimalsView += "</ul>";

//   allAnimalsDiv.innerHTML = allAnimalsView;
// };

// // Zadanie 11
// const createAnimal = async () => {
//   const name = animalNameForm.value;
//   const animalsCollectionRef = collection(database, "Zwierzeta");

//   await addDoc(animalsCollectionRef, { nazwa: name });
// };

// createAnimalBtn.addEventListener("click", createAnimal);

// // Zadanie 12

// const signInWithGoogle = async () => {
//   const googleAuthProvider = new GoogleAuthProvider();
//   signInWithPopup(auth, authProvider);
//   console.log(user);
// };

// signInWithGoogleBtn.addEventListener("click", signInWithGoogle);

// // OPERACJA 4: Nasłuchiwanie na zmianę statusu sesji użytkownika
// const authUserObserver = () => {
//   onAuthStateChanged(auth, async (user) => {
//     if (user) {
//       // Ktoś jest zalogowany..
//       viewForLoggedUser.style.display = "block";
//       viewForNotLoggedUser.style.display = "none";
//     } else {
//       // Ktoś nie jest zalogowany..
//       viewForLoggedUser.style.display = "none";
//       viewForNotLoggedUser.style.display = "block";
//     }
//   });
// };
// authUserObserver();

// ----------------------------------------------------------------

// Import funkcji związanych z obsługą Firebase'a
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";

import {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
  AuthErrorCodes,
  GoogleAuthProvider,
  GithubAuthProvider,
  signInWithPopup,
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-auth.js";

import {
  getFirestore,
  doc,
  getDoc,
  setDoc,
  addDoc,
  collection,
  query,
  getDocs,
  where,
  orderBy,
  limit,
  updateDoc,
  deleteDoc,
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-firestore.js";

// Przygtowanie konfiguracji projektu Firebase'a
const firebaseConfig = {
  apiKey: "AIzaSyDOQAsy4hmFfRdU0PVLpr9gZNPFYVFROsc",
  authDomain: "zdfronpol20-zwierzeta.firebaseapp.com",
  projectId: "zdfronpol20-zwierzeta",
  storageBucket: "zdfronpol20-zwierzeta.appspot.com",
  messagingSenderId: "1005707998234",
  appId: "1:1005707998234:web:2e53a10c8d0069e0e7cda3",
};

// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
// - uruchmienie modułu uwierzytelniania
const auth = getAuth(app);
// - uruchmienie modułu baz danych (firestore)
const database = getFirestore(app);

// Definiowanie elementów UI
const emailForm = document.querySelector("#emailForm");
const passwordForm = document.querySelector("#passwordForm");

const signUpBtn = document.querySelector("#signUpBtn");
const signInBtn = document.querySelector("#signInBtn");
const signInWithGoogleBtn = document.querySelector("#signInWithGoogleBtn");
const signOutBtn = document.querySelector("#signOutBtn");

const errorLabel = document.querySelector("#errorLabel");

const viewForNotLoggedUser = document.querySelector("#viewForNotLoggedUser");
const viewForLoggedUser = document.querySelector("#viewForLoggedUser");

const allAnimalsDiv = document.querySelector("#allAnimals");
const animalNameForm = document.querySelector("#animalNameForm");
const createAnimalBtn = document.querySelector("#createAnimalBtn");

// OPERACJA 1: Logowanie użytkownika
const signInUser = async () => {
  const valueEmail = emailForm.value;
  const valuePassword = passwordForm.value;

  try {
    const user = await signInWithEmailAndPassword(
      auth,
      valueEmail,
      valuePassword
    );

    // Zadanie 6:
    console.log(user.user.email);

    console.log("User logged in.");
  } catch {
    // Ewentualnie, gdyby skorzystać z catch(exception) to możemy wyjąć
    // później kod błędu (poprzez exception.code)
    // console.log(exception.code);
    errorLabel.innerHTML = "Podano nieprawidłowy email lub hasło";
  }
};

// Dodanie eventu do przycisku SignIn (wywołanie funkcji logoującej użytkownika)
signInBtn.addEventListener("click", signInUser);

// OPERACJA 2: Rejestracja użytkownika
const signUpUser = async () => {
  const valueEmail = emailForm.value;
  const valuePassword = passwordForm.value;

  try {
    const user = await createUserWithEmailAndPassword(
      auth,
      valueEmail,
      valuePassword
    );

    // Zadanie 7
    console.log(user);
  } catch (error) {
    if (error.code === "auth/email-already-in-use") {
      console.log("wybierz inny email, ten jest zajęty..");
    } else {
      console.log("Rejestracja nie powiodła się...");
    }
  }

  // :warning: WAŻNE :warning:
  // wskaźnik -> doc(database, "uzytkownicy", user.user.uid)
  // Dodanie danych do bazy danych (np. Firestore)
};

// Dodanie eventu do przycisku SignUp (wywołanie funkcji rejrestrującej użytkownika)
signUpBtn.addEventListener("click", signUpUser);

// OPERACJA 3: Wylogowanie użytkownika
const signOutUser = async () => {
  await signOut(auth);
  console.log("User logged out.");
};

// Dodanie eventu to przycisku SignOut (wywołanie funkcji wylogowującej)
signOutBtn.addEventListener("click", signOutUser);

// Zadanie 10
const generateAllAnimalsView = async () => {
  const allAnimalsQuery = query(collection(database, "zwierzeta"));
  const allAnimals = await getDocs(allAnimalsQuery);

  let allAnimalsView = "<ul>";

  allAnimals.forEach((animal) => {
    allAnimalsView += `<li>${animal.data().nazwa}</li>`;
  });

  allAnimalsView += "</ul>";

  allAnimalsDiv.innerHTML = allAnimalsView;
};

// Zadanie 11
const createAnimal = async () => {
  const name = animalNameForm.value;
  const animalsCollectionRef = collection(database, "zwierzeta");

  await addDoc(animalsCollectionRef, {
    nazwa: name,
  });

  generateAllAnimalsView();
};

createAnimalBtn.addEventListener("click", createAnimal);

// Zadanie 12
const signInWithGoogle = async () => {
  const authProvider = new GoogleAuthProvider();
  const user = await signInWithPopup(auth, authProvider);
  console.log(user);
};

signInWithGoogleBtn.addEventListener("click", signInWithGoogle);

// OPERACJA 4: Nasłuchiwanie na zmianę statusu sesji użytkownika
const authUserObserver = () => {
  onAuthStateChanged(auth, async (user) => {
    if (user) {
      // Ktoś jest zalogowany..
      viewForLoggedUser.style.display = "block";
      viewForNotLoggedUser.style.display = "none";

      generateAllAnimalsView();
    } else {
      // Ktoś nie jest zalogowany..
      viewForLoggedUser.style.display = "none";
      viewForNotLoggedUser.style.display = "block";
    }
  });
};
authUserObserver();
